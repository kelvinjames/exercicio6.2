
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import utfpr.ct.dainf.if62c.pratica.Jogador;
import utfpr.ct.dainf.if62c.pratica.JogadorComparator;
import utfpr.ct.dainf.if62c.pratica.Time;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * Template de projeto de programa Java usando Maven.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Pratica62 {
    public static void main(String[] args) {
        Time time1 = new Time();
        Time time2 = new Time();
        time1.addJogador("Goleiro",new Jogador(1,"Fulano"));
        time1.addJogador("Lateral",new Jogador(4,"Ciclano"));
        time1.addJogador("Atacante",new Jogador(4,"Beltrano"));
        time2.addJogador("Goleiro",new Jogador(1,"João"));
        time2.addJogador("Lateral",new Jogador(7,"José"));
        time2.addJogador("Atacante",new Jogador(15,"Mário"));
        Set<String> teste = time1.getJogadores().keySet();
        System.out.println("Posição"+ "    \t" + "Time 1" + "    \t " + "Time 2");
        for(String testes:teste){
            System.out.println(testes + "    \t" + time1.getJogadores().get(testes) + "\t " + time2.getJogadores().get(testes));
        }
        JogadorComparator comparador1 = new JogadorComparator(true,true,false);
        List<Jogador> lista1 = new ArrayList<>(time1.getJogadores().size());
        lista1 = time1.ordena(comparador1);
        for(Jogador a : lista1){
            System.out.println(a);
        }
        /*JogadorComparator comparador2 = new JogadorComparator(true,false,false);
        List<Jogador> lista2 = new ArrayList<>(time2.getJogadores().size());
        lista2 = time2.ordena(comparador2);
        for(Jogador b : lista2){
            System.out.println(b);
        }*/
        System.out.println(lista1.get(Collections.binarySearch(lista1,new Jogador(4,"Ciclano"),comparador1)));
    }
}
