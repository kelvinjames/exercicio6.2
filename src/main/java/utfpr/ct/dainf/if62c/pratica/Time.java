/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

import java.util.ArrayList;
import static java.util.Collections.sort;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author kelvin
 */
public class Time {
    private HashMap<String, Jogador> jogadores = new HashMap<>();
    public void addJogador(String chave,Jogador jogador){
        jogadores.put(chave,jogador);
    }
    public HashMap<String, Jogador> getJogadores() {
        return jogadores;
    }
    public List<Jogador> ordena(JogadorComparator comparador){
       ArrayList<Jogador> listaOrdenada = new ArrayList<>(jogadores.size());
       listaOrdenada.addAll(jogadores.values());
       sort(listaOrdenada,comparador);
       return listaOrdenada;
    }
}
